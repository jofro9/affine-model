import numpy as np

def findAvgPoint(coords, count):
    x = y = z = 0

    for i in range(count):
        x += coords[i, 0]
        y += coords[i, 1]
        z += coords[i, 2]

    return np.array([x / count, y / count, z / count])
