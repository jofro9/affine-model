from helpers import findAvgPoint

import copy
import numpy as np
import vtk

# vtk file reader
reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName("ReconstructedExternalSurface.vtp")
reader.Update()

data = reader.GetOutput()

# make copies for transforming
coords = np.array(data.GetPoints().GetData(), dtype = np.float32)
labels = np.array(data.GetPointData().GetArray("BoneLabel"))
tcoords = copy.deepcopy(coords)
tlabels = copy.deepcopy(labels)

# QC - labeler calls this node 0
tlabels[10804] = 4

# add 4th dimension to each point (x, y, z, 1)
count = data.GetNumberOfPoints()
tcoords = np.append(tcoords, [[1]] * count, axis = 1)

# structure to store coords by bone
tcoords_b = [[]] * int(max(tlabels))

for i in range(count):
    j = int(tlabels[i]) - 1
    tcoords_b[j].insert(0, tcoords[i])

# find center of control points, scale control points up (order matters here)
control_points = [[]] * int(max(tlabels))

for i in range(len(control_points)):
    control_points[i] = findAvgPoint(np.array(tcoords_b[i], dtype = np.float32), len(tcoords_b[i]))

control_points = np.array(control_points, dtype = np.float32)

center = findAvgPoint(control_points, len(control_points))
center = np.append(center, 1)

# build transformations
A = 2 * np.eye(4)
A[3, 3] = 1

# build translation based on control points
scaled_control_points = []
control_points = np.append(control_points, [[1]] * len(control_points), axis = 1)

for i in range(len(control_points)):
    scaled_control_points.insert(i, (control_points[i] - center) @ A + center)

T = []

for i in range(len(control_points)):
    w = np.eye(4, dtype = np.float32)
    p = scaled_control_points[i] - control_points[i]
    p[3] = 1

    for j in range(len(p)):
        w[j, 3] = p[j]

    T.insert(i, w)

# vtk objects for reconstruction
pts = vtk.vtkPoints()
pts.SetNumberOfPoints(len(tcoords))

# scale all coordinates based on new control points
for i in range(len(tcoords)):
    j = int(tlabels[i]) - 1
    A = T[j] @ A
    p = (tcoords[i] - control_points[j]) @ A + control_points[j]
    pts.SetPoint(i, p[0], p[1], p[2])

data.SetPoints(pts)

# vtk file writer
writer = vtk.vtkXMLPolyDataWriter()
writer.SetFileName("BoneTranformReconstructedExternalSurface.vtp")
writer.SetInputData(data)
writer.Update()
