import copy
import math
import numpy as np
import vtk

from helpers import findAvgPoint

reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName("ReconstructedExternalSurface.vtp")
reader.Update()

data = reader.GetOutput()

# print(data.GetPointData().GetArray("BoneLabel"))

# make copies for transforming
coords = np.array(data.GetPoints().GetData(), dtype = np.float32)
labels = np.array(data.GetPointData().GetArray("BoneLabel"))
tcoords = copy.deepcopy(coords)
tlabels = copy.deepcopy(labels)

# add 4th dimension to each point (x, y, z, 1)
# len = data.GetNumberOfPoints()
# ones = [[1]] * len 
# tcoords = np.append(tcoords, ones, axis = 1)

tcoords_o = []
tcoords_rp = []
tcoords_lp = []

for i in range(len(tcoords)):
    if tlabels[i] == 3:
        tcoords_rp.insert(0, tcoords[i])
    
    elif tlabels[i] == 4:
        tcoords_lp.insert(0, tcoords[i])

    elif tlabels[i] == 5:
        tcoords_o.insert(0, tcoords[i])

center_o = findAvgPoint(np.array(tcoords_o, dtype = np.float32), len(tcoords_o))
center_rp = findAvgPoint(np.array(tcoords_rp, dtype = np.float32), len(tcoords_rp))
center_lp = findAvgPoint(np.array(tcoords_lp, dtype = np.float32), len(tcoords_lp))

# vtk objects for reconstruction
pts = vtk.vtkPoints()
pts.SetNumberOfPoints(len(tcoords))

A = 2 * np.eye(3)
A[0, 0] = 1.25

for i in range(len(tcoords)):
    if tlabels[i] == 2 or tlabels[i] == 4 or tlabels[i] == 7 or (tlabels[i] == 5 and tcoords[i, 0] <= center_o[0]):
        # mag = math.sqrt( (center_rh[0] - tcoords[i, 0])**2 + (center_rh[1] - tcoords[i, 1])**2 + (center_rh[2] - tcoords[i, 2])**2 )
        # weight = 1 / (1 + math.exp(-mag))
        tcoords[i] = (tcoords[i] - center_rp ) @ A + center_rp
        pts.SetPoint(i, tcoords[i])

    else:
        # mag = math.sqrt( (center_lh[0] - tcoords[i, 0])**2 + (center_lh[1] - tcoords[i, 1])**2 + (center_lh[2] - tcoords[i, 2])**2 )
        # weight = 1 / (1 + math.exp(-mag))
        tcoords[i] = (tcoords[i] - center_lp ) @ A + center_lp
        pts.SetPoint(i, tcoords[i])

data.SetPoints(pts)

writer = vtk.vtkXMLPolyDataWriter()
writer.SetFileName("AnchoredTranformReconstructedExternalSurface.vtp")
writer.SetInputData(data)
writer.Update()