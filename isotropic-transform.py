import numpy as np
import vtk
import copy

from helpers import findAvgPoint

reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName("ReconstructedExternalSurface.vtp")
reader.Update()

data = reader.GetOutput()
coords = np.array(data.GetPoints().GetData(), dtype = np.float32)
tcoords = copy.deepcopy(coords)

A =  2 * np.eye(3)
count = data.GetNumberOfPoints()
center = findAvgPoint(tcoords, count)

pts = vtk.vtkPoints()
pts.SetNumberOfPoints(count)

for i in range(data.GetNumberOfPoints()):
    tcoords[i] = (tcoords[i] - center ) @ A + center
    pts.SetPoint(i, tcoords[i])

data.SetPoints(pts)

writer = vtk.vtkXMLPolyDataWriter()
writer.SetFileName("TransformedReconstructedExternalSurface.vtp")
writer.SetInputData(data)
writer.Update()
